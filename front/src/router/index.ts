import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

import FormVue from '@/views/Form.vue';
import AnswersVue from '@/views/Answers.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'List',
    component: AnswersVue,
  },
  {
    path: '/form',
    name: 'Form',
    component: FormVue,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
