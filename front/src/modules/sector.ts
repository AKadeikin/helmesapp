import { ISector } from '@/interfaces/sector';
import { reactive } from '@vue/reactivity';
import { toRefs } from 'vue';
import useApi from './api';

interface State {
  sectors: ISector[];
}

let sectors: ISector[] = [];
let initialized = false;

const state = reactive<State>({
  sectors: [],
});

export default function useSector() {
  const loadSectors = async () => {
    if (!initialized) {
      const apiGetSectors = useApi<ISector[]>('Sectors/parent-child', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });

      await apiGetSectors.request();

      if (apiGetSectors.response.value) {
        sectors = apiGetSectors.response.value!;
        state.sectors = sectors;
      }

      initialized = true;
    }
  };

  return { ...toRefs(state), loadSectors };
}
