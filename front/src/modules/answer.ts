import { IAnswer } from '@/interfaces/answer';
import { IAddAnswerRequest } from '@/interfaces/requests/addAnswerRequest';
import { reactive, toRefs } from '@vue/reactivity';
import useApi, { useApiRawRequest } from './api';

interface State {
  answers: IAnswer[];
}

let answers: IAnswer[] = [];
let initialized = false;

const state = reactive<State>({
  answers: [],
});

export default function useAnswer() {
  const loadAnswers = async () => {
    if (!initialized) {
      const apiGetAnswers = useApi<IAnswer[]>('Answers', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });

      await apiGetAnswers.request();

      if (apiGetAnswers.response.value) {
        answers = apiGetAnswers.response.value!;
        state.answers = answers;
      }

      initialized = true;
    }
  };

  const addAnswer = async (answer: IAddAnswerRequest) => {
    const apiAddAnswer = useApi<IAddAnswerRequest>('Answers', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(answer),
    });

    await apiAddAnswer.request();

    if (apiAddAnswer.response.value) {
      answers.push(<IAnswer>(<unknown>apiAddAnswer.response.value!));
      state.answers = answers;
    }
  };

  const editAnswer = async (answerId: number, answer: IAddAnswerRequest) => {
    const apiEditAnswer = useApi<IAddAnswerRequest>('Answers/' + answerId, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(answer),
    });

    await apiEditAnswer.request();

    initialized = false;
    loadAnswers();
  };

  const deleteAnswer = async (answerId: number) => {
    const apiDeleteAnswer = useApiRawRequest('Answers/' + answerId, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    await apiDeleteAnswer();

    initialized = false;
    loadAnswers();
  };

  return { ...toRefs(state), loadAnswers, addAnswer, editAnswer, deleteAnswer };
}
