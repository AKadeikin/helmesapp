import { Ref } from 'vue';
import { ApiRequest } from '@/modules/api';

export interface IUsableAPI<T> {
  response: Ref<T | undefined>;
  request: ApiRequest;
}
