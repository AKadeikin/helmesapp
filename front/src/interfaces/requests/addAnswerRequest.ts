export interface IAddAnswerRequest {
  name: string;
  sectorIDs: number[];
  agreedWithTerms: boolean;
}
