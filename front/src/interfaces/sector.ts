export interface ISector {
  id: number;
  name: string;
  subSectors: ISector[];
}
