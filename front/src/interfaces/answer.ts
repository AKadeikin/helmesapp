import { ISector } from '@/interfaces/sector';

export interface IAnswer {
  id?: number;
  name: string;
  sectors: ISector[];
  agreedWithTerms: boolean;
}
