﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HelmesApp.Data;
using HelmesApp.Models;
using HelmesApp.Models.Requests;

namespace HelmesApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswersController : ControllerBase
    {
        private readonly AppDbContext _context;

        public AnswersController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Answers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Answer>>> GetAnswers()
        {
            return await _context.Answers.Include(x => x.Sectors).ToListAsync();
        }

        // GET: api/Answers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Answer>> GetAnswer(int id)
        {
            var answer = await _context.Answers.FindAsync(id);

            if (answer == null)
            {
                return NotFound();
            }

            return answer;
        }

        // PUT: api/Answers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAnswer(int id, EditAnswerRequest request)
        {
            var answerInContext = _context.Answers.FindAsync(id).Result;
            
            if (answerInContext == null)
            {
                return BadRequest();
            }

            answerInContext.Name = request.Name;
            answerInContext.Sectors = new List<Sector>();
            request.SectorIDs.ToList().ForEach(id => answerInContext.Sectors.Add(_context.Sectors.FindAsync(id).Result));
            answerInContext.AgreedWithTerms = request.AgreedWithTerms;

            _context.Entry(answerInContext).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnswerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Answers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Answer>> PostAnswer(AddAnswerRequest request)
        {
            if (!Regex.IsMatch(request.Name, @"^[a-zA-Z]+$"))
            {
                return BadRequest("Name must contain only letters");
            }

            if (request.SectorIDs.Count == 0)
            {
                return BadRequest("Please select at least 1 sector");
            }

            Answer answer = new Answer {Name = request.Name, Sectors = new List<Sector>(), AgreedWithTerms = request.AgreedWithTerms};
            request.SectorIDs.ToList().ForEach(id => answer.Sectors.Add(_context.Sectors.FindAsync(id).Result)); 
            _context.Answers.Add(answer);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAnswer", new { id = answer.Id }, answer);
        }

        // DELETE: api/Answers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAnswer(int id)
        {
            var answer = await _context.Answers.FindAsync(id);
            if (answer == null)
            {
                return NotFound();
            }

            _context.Answers.Remove(answer);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AnswerExists(int id)
        {
            return _context.Answers.Any(e => e.Id == id);
        }
    }
}
