﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelmesApp.Data;
using HelmesApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HelmesApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SectorsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public SectorsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Sectors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Sector>>> GetSectors()
        {
            return await _context.Sectors.ToListAsync();
        }

        // GET: api/Sectors/Parent-Child
        [HttpGet("parent-child")]
        public async Task<ActionResult<IEnumerable<Sector>>> GetSectorsParentChild()
        {
            var parents = await _context.Sectors.Include(x => x.ChildrenSectors).Where(x => !x.ParentSectors.Any()).ToListAsync();

            Iteration(parents);

            return parents;
        }

        // GET: api/Sectors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sector>> GetSector(int id)
        {
            var sector = await _context.Sectors.FindAsync(id);

            if (sector == null) return NotFound();

            return sector;
        }

        // PUT: api/Sectors/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSector(int id, Sector sector)
        {
            if (id != sector.Id) return BadRequest();

            _context.Entry(sector).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SectorExists(id))
                    return NotFound();
                throw;
            }

            return NoContent();
        }

        // POST: api/Sectors
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Sector>> PostSector(Sector sector)
        {
            _context.Sectors.Add(sector);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSector", new {id = sector.Id}, sector);
        }

        // DELETE: api/Sectors/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSector(int id)
        {
            var sector = await _context.Sectors.FindAsync(id);
            if (sector == null) return NotFound();

            _context.Sectors.Remove(sector);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SectorExists(int id)
        {
            return _context.Sectors.Any(e => e.Id == id);
        }

        private void Iteration(List<Sector> list)
        {
            if (list.Count != 0)
            {
                list.ForEach(x =>
                {
                    var sector = _context.Sectors.Include(x => x.ChildrenSectors).FirstOrDefaultAsync(y => y.Id == x.Id);
                    sector.Result.SubSectors = MakeListFromIDs(x.ChildrenSectors.Select(y => y.ChildrenId).ToList());
                    Iteration(x.SubSectors);
                });
            }
        }

        private List<Sector> MakeListFromIDs(List<int> inputList)
        {
            var outputList = new List<Sector>();

            inputList.ForEach(x => outputList.Add(_context.Sectors.FindAsync(x).Result));

            return outputList;
        }
    }
}