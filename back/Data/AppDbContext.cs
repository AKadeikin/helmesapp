﻿using System.Collections.Generic;
using HelmesApp.Models;
using Microsoft.EntityFrameworkCore;

namespace HelmesApp.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Sector> Sectors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Answer>().HasKey(x => x.Id);
            modelBuilder.Entity<Sector>().HasKey(x => x.Id);
            modelBuilder.Entity<SectorsRelation>().HasKey(x => new {x.ParentId, x.ChildrenId});

            modelBuilder.Entity<SectorsRelation>()
                .HasOne(x => x.ParentSector)
                .WithMany(x => x.ChildrenSectors)
                .HasForeignKey(x => x.ParentId);
            modelBuilder.Entity<SectorsRelation>()
                .HasOne(x => x.ChildrenSector)
                .WithMany(x => x.ParentSectors)
                .HasForeignKey(x => x.ChildrenId);

            //modelBuilder.Entity<Answer>().HasMany(x => x.Sectors).WithMany(x => x.Answers);

            modelBuilder.Entity<Sector>().HasData(
                new Sector {Id = 1, Name = "Manufacturing"},
                new Sector {Id = 2, Name = "Construction materials"},
                new Sector {Id = 3, Name = "Electronics and Optics"},
                new Sector {Id = 4, Name = "Food and Beverage"},
                new Sector {Id = 5, Name = "Bakery & confectionery products"},
                new Sector {Id = 6, Name = "Beverages"},
                new Sector {Id = 7, Name = "Fish & fish products"},
                new Sector {Id = 8, Name = "Meat & meat products"},
                new Sector {Id = 9, Name = "Milk & dairy products"},
                new Sector {Id = 10, Name = "Sweets & snack food"},
                new Sector {Id = 11, Name = "Other"},
                new Sector {Id = 12, Name = "Furniture"},
                new Sector {Id = 13, Name = "Bathroom/sauna"},
                new Sector {Id = 14, Name = "Bedroom"},
                new Sector {Id = 15, Name = "Children’s room"},
                new Sector {Id = 16, Name = "Kitchen"},
                new Sector {Id = 17, Name = "Living room"},
                new Sector {Id = 18, Name = "Office"},
                new Sector {Id = 19, Name = "Outdoor"},
                new Sector {Id = 20, Name = "Project furniture"},
                new Sector {Id = 21, Name = "Other (Furniture)"},
                new Sector {Id = 22, Name = "Machinery"},
                new Sector {Id = 23, Name = "Machinery components"},
                new Sector {Id = 24, Name = "Machinery equipment/tools"},
                new Sector {Id = 25, Name = "Manufacture of machinery"},
                new Sector {Id = 26, Name = "Maritime"},
                new Sector {Id = 27, Name = "Aluminium and steel workboats"},
                new Sector {Id = 28, Name = "Boat/Yacht building"},
                new Sector {Id = 29, Name = "Ship repair and conversion"},
                new Sector {Id = 30, Name = "Metal structures"},
                new Sector {Id = 31, Name = "Repair and maintenance service"},
                new Sector {Id = 32, Name = "Other"},
                new Sector {Id = 33, Name = "Metalworking"},
                new Sector {Id = 34, Name = "Construction of metal structures"},
                new Sector {Id = 35, Name = "Houses and buildings"},
                new Sector {Id = 36, Name = "Metal products"},
                new Sector {Id = 37, Name = "Metal works"},
                new Sector {Id = 38, Name = "CNC-machining"},
                new Sector {Id = 39, Name = "Forgings, Fasteners"},
                new Sector {Id = 40, Name = "Gas, Plasma, Laser cutting"},
                new Sector {Id = 41, Name = "MIG, TIG, Aluminum welding"},
                new Sector {Id = 42, Name = "Plastic and Rubber"},
                new Sector {Id = 43, Name = "Packaging"},
                new Sector {Id = 44, Name = "Plastic goods"},
                new Sector {Id = 45, Name = "Plastic processing technology"},
                new Sector {Id = 46, Name = "Blowing"},
                new Sector {Id = 47, Name = "Moulding"},
                new Sector {Id = 48, Name = "Plastics welding and processing"},
                new Sector {Id = 49, Name = "Plastic profiles"},
                new Sector {Id = 50, Name = "Printing"},
                new Sector {Id = 51, Name = "Advertising"},
                new Sector {Id = 52, Name = "Book/Periodicals printing"},
                new Sector {Id = 53, Name = "Labelling and packaging printing"},
                new Sector {Id = 54, Name = "Textile and Clothing"},
                new Sector {Id = 55, Name = "Clothing"},
                new Sector {Id = 56, Name = "Textile"},
                new Sector {Id = 57, Name = "Wood"},
                new Sector {Id = 58, Name = "Wooden building materials"},
                new Sector {Id = 59, Name = "Wooden houses"},
                new Sector {Id = 60, Name = "Other (Wood)"},
                new Sector {Id = 61, Name = "Service"},
                new Sector {Id = 62, Name = "Business services"},
                new Sector {Id = 63, Name = "Engineering"},
                new Sector {Id = 64, Name = "Information Technology and Telecommunications"},
                new Sector {Id = 65, Name = "Data processing, Web portals, E-marketing"},
                new Sector {Id = 66, Name = "Programming, Consultancy"},
                new Sector {Id = 67, Name = "Software, Hardware"},
                new Sector {Id = 68, Name = "Telecommunications"},
                new Sector {Id = 69, Name = "Tourism"},
                new Sector {Id = 70, Name = "Translation services"},
                new Sector {Id = 71, Name = "Transport and Logistics"},
                new Sector {Id = 72, Name = "Air"},
                new Sector {Id = 73, Name = "Rail"},
                new Sector {Id = 74, Name = "Road"},
                new Sector {Id = 75, Name = "Water"},
                new Sector {Id = 76, Name = "Other"},
                new Sector {Id = 77, Name = "Creative industries"},
                new Sector {Id = 78, Name = "Energy technology"},
                new Sector {Id = 79, Name = "Environment"});

            modelBuilder.Entity<SectorsRelation>().HasData(
                new SectorsRelation {ParentId = 1, ChildrenId = 2},
                new SectorsRelation {ParentId = 1, ChildrenId = 3},
                new SectorsRelation {ParentId = 1, ChildrenId = 4},
                new SectorsRelation {ParentId = 4, ChildrenId = 5},
                new SectorsRelation {ParentId = 4, ChildrenId = 6},
                new SectorsRelation {ParentId = 4, ChildrenId = 7},
                new SectorsRelation {ParentId = 4, ChildrenId = 8},
                new SectorsRelation {ParentId = 4, ChildrenId = 9},
                new SectorsRelation {ParentId = 4, ChildrenId = 10},
                new SectorsRelation {ParentId = 4, ChildrenId = 11},
                new SectorsRelation {ParentId = 1, ChildrenId = 12},
                new SectorsRelation {ParentId = 12, ChildrenId = 13},
                new SectorsRelation {ParentId = 12, ChildrenId = 14},
                new SectorsRelation {ParentId = 12, ChildrenId = 15},
                new SectorsRelation {ParentId = 12, ChildrenId = 16},
                new SectorsRelation {ParentId = 12, ChildrenId = 17},
                new SectorsRelation {ParentId = 12, ChildrenId = 18},
                new SectorsRelation {ParentId = 12, ChildrenId = 19},
                new SectorsRelation {ParentId = 12, ChildrenId = 20},
                new SectorsRelation {ParentId = 12, ChildrenId = 21},
                new SectorsRelation {ParentId = 1, ChildrenId = 22},
                new SectorsRelation {ParentId = 22, ChildrenId = 23},
                new SectorsRelation {ParentId = 22, ChildrenId = 24},
                new SectorsRelation {ParentId = 22, ChildrenId = 25},
                new SectorsRelation {ParentId = 22, ChildrenId = 26},
                new SectorsRelation {ParentId = 26, ChildrenId = 27},
                new SectorsRelation {ParentId = 26, ChildrenId = 28},
                new SectorsRelation {ParentId = 26, ChildrenId = 29},
                new SectorsRelation {ParentId = 22, ChildrenId = 30},
                new SectorsRelation {ParentId = 22, ChildrenId = 31},
                new SectorsRelation {ParentId = 22, ChildrenId = 32},
                new SectorsRelation {ParentId = 1, ChildrenId = 33},
                new SectorsRelation {ParentId = 33, ChildrenId = 34},
                new SectorsRelation {ParentId = 33, ChildrenId = 35},
                new SectorsRelation {ParentId = 33, ChildrenId = 36},
                new SectorsRelation {ParentId = 33, ChildrenId = 37},
                new SectorsRelation {ParentId = 37, ChildrenId = 38},
                new SectorsRelation {ParentId = 37, ChildrenId = 39},
                new SectorsRelation {ParentId = 37, ChildrenId = 40},
                new SectorsRelation {ParentId = 37, ChildrenId = 41},
                new SectorsRelation {ParentId = 1, ChildrenId = 42},
                new SectorsRelation {ParentId = 42, ChildrenId = 43},
                new SectorsRelation {ParentId = 42, ChildrenId = 44},
                new SectorsRelation {ParentId = 42, ChildrenId = 45},
                new SectorsRelation {ParentId = 45, ChildrenId = 46},
                new SectorsRelation {ParentId = 45, ChildrenId = 47},
                new SectorsRelation {ParentId = 45, ChildrenId = 48},
                new SectorsRelation {ParentId = 42, ChildrenId = 49},
                new SectorsRelation {ParentId = 1, ChildrenId = 50},
                new SectorsRelation {ParentId = 50, ChildrenId = 51},
                new SectorsRelation {ParentId = 50, ChildrenId = 52},
                new SectorsRelation {ParentId = 50, ChildrenId = 53},
                new SectorsRelation {ParentId = 1, ChildrenId = 54},
                new SectorsRelation {ParentId = 54, ChildrenId = 55},
                new SectorsRelation {ParentId = 54, ChildrenId = 56},
                new SectorsRelation {ParentId = 1, ChildrenId = 57},
                new SectorsRelation {ParentId = 57, ChildrenId = 58},
                new SectorsRelation {ParentId = 57, ChildrenId = 59},
                new SectorsRelation {ParentId = 57, ChildrenId = 60},
                new SectorsRelation {ParentId = 61, ChildrenId = 62},
                new SectorsRelation {ParentId = 61, ChildrenId = 63},
                new SectorsRelation {ParentId = 61, ChildrenId = 64},
                new SectorsRelation {ParentId = 64, ChildrenId = 65},
                new SectorsRelation {ParentId = 64, ChildrenId = 66},
                new SectorsRelation {ParentId = 64, ChildrenId = 67},
                new SectorsRelation {ParentId = 64, ChildrenId = 68},
                new SectorsRelation {ParentId = 61, ChildrenId = 69},
                new SectorsRelation {ParentId = 61, ChildrenId = 70},
                new SectorsRelation {ParentId = 61, ChildrenId = 71},
                new SectorsRelation {ParentId = 71, ChildrenId = 72},
                new SectorsRelation {ParentId = 71, ChildrenId = 73},
                new SectorsRelation {ParentId = 71, ChildrenId = 74},
                new SectorsRelation {ParentId = 71, ChildrenId = 75},
                new SectorsRelation {ParentId = 76, ChildrenId = 77},
                new SectorsRelation {ParentId = 76, ChildrenId = 78},
                new SectorsRelation {ParentId = 76, ChildrenId = 79}
            );

            modelBuilder.Entity<Answer>().HasData(
                new Answer
                {
                    Id = 1,
                    Name = "Aleksei",
                    Sectors = new List<Sector>(),
                    AgreedWithTerms = true
                });
        }
    }
}