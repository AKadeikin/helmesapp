﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace HelmesApp.Models
{
    public class Sector
    {
        public int Id { get; init; }
        public string Name { get; init; }
        public List<Sector> SubSectors { get; set; }

        [JsonIgnore]
        public virtual IEnumerable<Answer> Answers { get; set; }
        [JsonIgnore]
        public virtual IEnumerable<SectorsRelation> ParentSectors { get; set; }
        [JsonIgnore]
        public virtual IEnumerable<SectorsRelation> ChildrenSectors { get; set; }
    }
}
