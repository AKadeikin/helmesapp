﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelmesApp.Models.Responses
{
    public class GetParentChildSectorsResponse
    {
        public int Id { get; init; }
        public string Name { get; init; }
        public List<GetParentChildSectorsResponse> ChildSectors { get; init; }
    }
}
