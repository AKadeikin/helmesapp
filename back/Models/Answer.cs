﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelmesApp.Models
{
    public class Answer
    {
        public int Id { get; init; }
        [Required]
        public string Name { get; set; }
        [Required]
        public List<Sector> Sectors { get; set; }
        [Required]
        public bool AgreedWithTerms { get; set; }
    }
}
