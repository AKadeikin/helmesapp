﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HelmesApp.Models
{
    public class SectorsRelation
    {
        public int ParentId { get; set; }
        public int ChildrenId { get; set; }

        [JsonIgnore]
        public virtual Sector ParentSector { get; set; }
        [JsonIgnore]
        public virtual Sector ChildrenSector { get; set; }
    }
}
