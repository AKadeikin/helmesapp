﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelmesApp.Models.Requests
{
    public class AddAnswerRequest
    {
        [Required]
        public string Name { get; init; }
        [Required]
        public List<int> SectorIDs { get; init; }
        [Required]
        public bool AgreedWithTerms { get; init; }
    }
}
